# Final Project QnA App



## Kelompok 8

## Anggota Kelompok
- Diki Sofyana Arbi
- Nabila Ayu Puspita

## Tema Project

Forum Tanya Jawab
## ERD
https://gitlab.com/sofyandiki4/final-project-qna-app/-/blob/nabilayups/public/images/erd.jpg

## LINK VIDEO
 Link demo Youtube : https://youtu.be/UDwC3pRG5K8 

## LINK WEBSITE
https://dikisofyana.sanbercodeapp.com/login
