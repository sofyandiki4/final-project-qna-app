<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            [
                'username' => 'User Test',
                'email' => 'user@test.test',
                'password' => Hash::make('password'),
            ],
        ])->each(function($users){
            User::create($users);
        });
    }
}
