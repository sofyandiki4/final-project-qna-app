<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ThreadController;
use App\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| comment
*/

// Route::get('/', function () {
//     return view('non-users.home');
// });

// Route::get('/category', function () {
//     return view('non-users.category');
// });

// Route::get('/thread', function () {
//     return view('non-users.thread');
// });

Route::get('/register', function () {
    return view('auths.register');
});
Route::post('/register',[UserController::class,'register']);

Route::get('/login', function () {
    return view('auths.login');
});
Route::post('/login',[UserController::class,'login']);
 
Route::get('/tambah', function () {
    return view('users.tambah');
});


// Route::get('/index',[ThreadController::class,'indexHome']);


// Route::get('/thread',[ThreadController::class,'indexThread']);
// Route::post('/threads',[ThreadController::class,'storeThread']);
// Route::get('/thread/create',[ThreadController::class,'createThread']);
// Route::get('/thread/{id}',[ThreadController::class,'showThread']);
// Route::get('/thread/{id}/edit',[ThreadController::class,'editThread']);
// Route::put('/thread/{id}',[ThreadController::class,'updateThread']);
// Route::delete('/thread/{id}',[ThreadController::class,'destroyThread']);


Route::middleware('auth')->group(function(){
    Route::get('/logout',[UserController::class,'logout']);
    // Route::get('/home',function(){
    //     return view('users.home');
    // });


    
    // // Route::post('/thread',[ThreadController::class,'getPayload']);
    
    // Route::get('/edit-profile', function () {
    //     return view('users.edit-profil');
    // });
    
    
    // Route::get('/profile', function () {
    //     return view('users.profil');
    // });

    Route::get('/index',[ThreadController::class,'indexHome']);


// Route::get('/thread',[ThreadController::class,'indexThread']);
Route::post('/threads',[ThreadController::class,'storeThread']);
Route::get('/thread/create',[ThreadController::class,'createThread']);
Route::get('/thread/{id}',[ThreadController::class,'showThread']);
Route::get('/thread/{id}/edit',[ThreadController::class,'editThread']);
Route::put('/thread/{id}',[ThreadController::class,'updateThread']);
Route::delete('/thread/{id}',[ThreadController::class,'destroyThread']);


Route::post('/comment/{id}',[CommentController::class,'storeComment']);
Route::get('/comment/create',[CommentController::class,'createComment']);

});
