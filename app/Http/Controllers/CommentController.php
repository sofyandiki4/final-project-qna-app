<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Jawaban;
use App\Models\Kategori;
use App\Models\Pertanyaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{



    

    public function createComment() {

        // $pertanyaan = Pertanyaan::find($id);
        $user = User::all();
        $kategori = Kategori::all();
        return view('non-users.tambah-comment',[ 'user'=> $user, 'kategori'=> $kategori]);

    }

    
    public function storeComment(Request $request,$id) {

        
        $request->validate([

            'jawab'  => 'required',

        ],
        [
            // 'name.required' => "nama tidak boleh kosong",
            // 'gameplay.required' => "gameplay tidak boleh kosong",
            // 'developer.required' => "developer tidak boleh kosong",
            // 'year.required' => "umur tidak boleh kosong",
            // 'year.integer' => "umur harus angka",
        ]);
       

        $jawaban = new jawaban;
        $jawaban->jawab = $request->jawab;
        $jawaban->user_id = Auth::user()->id;
        $jawaban->pertanyaan_id = $id;
        
        

        $jawaban->save();

        return redirect('/thread/'. $id);
    }

    public function editThread($id) {

        
        $jawaban = Jawaban::find($id);
        return view('non-users.edit-comment',['jawaban' => $jawaban]);

    }

    public function updateThread(Request $request,$id) {

        
        $request->validate([
            'jawab' => 'required',
            
        ],
        [
            // 'name.required' => "nama tidak boleh kosong",
            // 'gameplay.required' => "gameplay tidak boleh kosong",
            // 'developer.required' => "developer tidak boleh kosong",
            // 'year.required' => "umur tidak boleh kosong",
            // 'year.integer' => "umur harus angka",
        ]);

        // DB::table('pertanyaan')->insert([
        //     'judul' => $request['judul'],
        //     'konten' => $request['konten'],
        //     'kategori' => $request['kategori_Id'],
        // ]
        // );

        $jawaban =  Jawaban::find($id);
        $jawaban->jawab = $request->jawab;
        
        

        $jawaban->save();

        return 'succes';
    }

    public function destroyComment($id) {

        
        $jawaban = Jawaban::find($id)->delete();

        
        // $kategori = DB::table('kategori')->get();
        return view('non-users.home',[ 'jawaban'=>$jawaban]);

    }
}
