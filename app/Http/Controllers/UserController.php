<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $payload = $this->validate($request,[
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8',
            'captcha' => ['required','captcha'],
        ]);
        

      
        $payload['password'] = Hash::make($payload['password']);
        User::create($payload);
        return redirect('login');
    }



    public function login(Request $request)
    {
        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required|min:8'
        ]);
        $user = User::where('email',$request->email)->first();
        if(!$user){
            return redirect()->back();
        }
        if(!Hash::check($request->password,$user->password)){
            return redirect()->back();
        }
        Auth::login($user);
        return redirect('index');
    }

    public function logout()
    {
        if(Auth::check()){
            $user = request()->user();
            Auth::logout($user);
            return redirect('login');
        }
        return redirect()->back();
    }
}
