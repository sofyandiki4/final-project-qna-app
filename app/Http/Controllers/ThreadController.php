<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Kategori;
use App\Models\Pertanyaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
// use file;

class ThreadController extends Controller
{
    public function getPayload(Request $request)
    {
        $this->validate($request, [
	    	// 'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);




        $image = $request->file('image');
        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
     
   
        $destinationPath = public_path('/thumbnail');

        if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 666, true);
        }
        $img = Image::make($image->getRealPath());
        $img->resize(100, 100, function ($constraint) {
		    $constraint->aspectRatio();
		})->save($destinationPath.'/'.$input['imagename']);


        $destinationPath = public_path('/images');
        $image->move($destinationPath, $input['imagename']);


        // $this->postImage->add($input);


        // 
        return 'Success';
    }

    public function indexHome() {

        $kategori = Kategori::all();
        $pertanyaan = Pertanyaan::all();
        // $kategori = DB::table('kategori')->get();
        return view('non-users.home',['kategori' => $kategori,'pertanyaan'=>$pertanyaan]);

    }


    

    public function createThread() {

        $kategori = Kategori::all();
        $user = User::all();
        return view('non-users.tambah-thread',['kategori' => $kategori, 'user'=> $user]);

    }

    public function showThread($id) {

        $kategori = Kategori::all();
        $pertanyaan = Pertanyaan::find($id);
        

        
        // $kategori = DB::table('kategori')->get();
        return view('non-users.thread',['kategori' => $kategori, 'pertanyaan'=>$pertanyaan]);

    }

    public function storeThread(Request $request) {

        
        $request->validate([
            'judul' => 'required',
            'konten'  => 'required',
            'img'  => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            // 'user_id' => 'required',
            'kategori_Id' => 'required',
        ],
        [
            // 'name.required' => "nama tidak boleh kosong",
            // 'gameplay.required' => "gameplay tidak boleh kosong",
            // 'developer.required' => "developer tidak boleh kosong",
            // 'year.required' => "umur tidak boleh kosong",
            // 'year.integer' => "umur harus angka",
        ]);

        // DB::table('pertanyaan')->insert([
        //     'judul' => $request['judul'],
        //     'konten' => $request['konten'],
        //     'kategori' => $request['kategori_Id'],
        // ]
        // );

        $image = $request->file('img');
        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
     
   
        $destinationPath = public_path('/thumbnail');

        if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 666, true);
        }
        $img = Image::make($image->getRealPath());
        $img->resize(100, 100, function ($constraint) {
		    $constraint->aspectRatio();
		})->save($destinationPath.'/'.$input['imagename']);


        $destinationPath = public_path('/images');
        $image->move($destinationPath, $input['imagename']);

        $pertanyaan = new Pertanyaan;
        $pertanyaan->judul = $request->judul;
        $pertanyaan->konten = $request->konten;
        $pertanyaan->kategori_Id = $request->kategori_Id;
        $pertanyaan->img = $input['imagename'];
        $pertanyaan->user_id = Auth::user()->id;
        // $pertanyaan->user_id = request()->user()->id;
        
        

        $pertanyaan->save();

        return redirect('/index');
    }

    public function editThread($id) {

        $kategori = Kategori::all();
        $pertanyaan = Pertanyaan::find($id);
        return view('non-users.edit-thread',['kategori' => $kategori, 'pertanyaan'=>$pertanyaan]);

    }

    public function updateThread(Request $request,$id) {

        
        $request->validate([
            'judul' => 'required',
            'konten'  => 'required',
            'img'  => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'kategori_Id' => 'required',
        ],
        [
            // 'name.required' => "nama tidak boleh kosong",
            // 'gameplay.required' => "gameplay tidak boleh kosong",
            // 'developer.required' => "developer tidak boleh kosong",
            // 'year.required' => "umur tidak boleh kosong",
            // 'year.integer' => "umur harus angka",
        ]);

        // DB::table('pertanyaan')->insert([
        //     'judul' => $request['judul'],
        //     'konten' => $request['konten'],
        //     'kategori' => $request['kategori_Id'],
        // ]
        // );

        $pertanyaan =  Pertanyaan::find($id);
        $pertanyaan->judul = $request->judul;
        $pertanyaan->konten = $request->konten;
        $pertanyaan->kategori_Id = $request->kategori_Id;

        if ($request->has('img')) {
            $destinationPath = public_path('/thumbnail');
            File::delete($destinationPath. $pertanyaan->img);
            $destinationPath = public_path('/images');
            File::delete($destinationPath. $pertanyaan->img);

        $image = $request->file('img');
        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
     
        
   
        $destinationPath = public_path('/thumbnail');

        if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 666, true);
        }
        $img = Image::make($image->getRealPath());
        $img->resize(100, 100, function ($constraint) {
		    $constraint->aspectRatio();
		})->save($destinationPath.'/'.$input['imagename']);


        $destinationPath = public_path('/images');
        $image->move($destinationPath, $input['imagename']);
        $pertanyaan->img = $input['imagename'];
        }
        
        
        
        

        $pertanyaan->save();

        return 'succes';
    }

    public function destroyThread($id) {

        $kategori = Kategori::all();
        $pertanyaan = Pertanyaan::find($id)->delete();

        
        // $kategori = DB::table('kategori')->get();
        return view('non-users.home',['kategori' => $kategori, 'pertanyaan'=>$pertanyaan]);

    }


}
