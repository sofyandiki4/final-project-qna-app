<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = 'pertanyaan';   
    protected $fillable = ['judul','img','konten','user_id','kategori_Id']; 
    use HasFactory;

    public function user()
  {
   return $this->belongsTo(User::class);
  }  

  public function kategori()
  {
   return $this->belongsTo(Kategori::class);
  }  

  public function jawaban()
  {
   return $this->hasMany(Jawaban::class);
  }  
}


