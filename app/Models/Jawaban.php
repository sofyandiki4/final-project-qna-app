<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    protected $table = 'jawaban';   
    protected $fillable = ['pertanyaan_id','user_id','jawab']; 
    use HasFactory;

    public function user()
    {
     return $this->belongsTo(User::class);
    }  
  
    public function pertanyaan()
    {
     return $this->belongsTo(Pertanyaan::class);
    }  
  

}
