<div class="col-12 col-xl-4 col-xxl-3">
    <div class="row">
      <!-- Mailing List Start -->
      <div class="col-12">
        <div class="card mb-5">
          @yield('leftrow-top')
        </div>
      </div>
      <!-- Mailing List End -->


      <!-- Categories Start -->
      <div class="col-12 col-sm-6 col-xl-12">
        <h2 class="small-title">Categories</h2>
        <div class="card mb-5">
          <div class="card-body">
            <div class="row g-0">
              <div class="col-12 col-sm-6 mb-n2">
                @forelse ($kategori as $key => $item)
                <a href="Pages.Blog.List.html" class="body-link d-block mb-2">{{ $item->nama }}</a>
                @empty
                <a href="Pages.Blog.List.html" class="body-link d-block mb-2">kosong</a>
                @endforelse
              </div>

            </div>
          </div>
        </div>
      </div>
      <!-- Categories End -->

      <!-- Tags Start -->
      <div class="col-12 col-sm-6 col-xl-12">
        <h2 class="small-title">Tags</h2>
        <div class="card mb-5">
          <div class="card-body">

            <a class="btn btn-sm btn-icon btn-icon-end btn-outline-primary mb-1 me-1" href="Pages.Blog.List.html">
              <span>Food (12)</span>
            </a>
            <a class="btn btn-sm btn-icon btn-icon-end btn-outline-primary mb-1 me-1" href="Pages.Blog.List.html">
              <span>Baking (3)</span>
            </a>
            <a class="btn btn-sm btn-icon btn-icon-end btn-outline-primary mb-1 me-1" href="Pages.Blog.List.html">
              <span>Sweet (1)</span>
            </a>
            <a class="btn btn-sm btn-icon btn-icon-end btn-outline-primary mb-1 me-1" href="Pages.Blog.List.html">
              <span>Molding (3)</span>
            </a>
            <a class="btn btn-sm btn-icon btn-icon-end btn-outline-primary mb-1 me-1" href="Pages.Blog.List.html">
              <span>Pastry (5)</span>
            </a>
            <a class="btn btn-sm btn-icon btn-icon-end btn-outline-primary mb-1 me-1" href="Pages.Blog.List.html">
              <span>Healthy (7)</span>
            </a>
            <a class="btn btn-sm btn-icon btn-icon-end btn-outline-primary mb-1 me-1" href="Pages.Blog.List.html">
              <span>Rye (3)</span>
            </a>
            <a class="btn btn-sm btn-icon btn-icon-end btn-outline-primary mb-1 me-1" href="Pages.Blog.List.html">
              <span>Simple (3)</span>
            </a>
            <a class="btn btn-sm btn-icon btn-icon-end btn-outline-primary mb-1 me-1" href="Pages.Blog.List.html">
              <span>Cake (2)</span>
            </a>
            <a class="btn btn-sm btn-icon btn-icon-end btn-outline-primary mb-1 me-1" href="Pages.Blog.List.html">
              <span>Recipe (6)</span>
            </a>
            <a class="btn btn-sm btn-icon btn-icon-end btn-outline-primary mb-1 me-1" href="Pages.Blog.List.html">
              <span>Bread (8)</span>
            </a>
            <a class="btn btn-sm btn-icon btn-icon-end btn-outline-primary mb-1 me-1" href="Pages.Blog.List.html">
              <span>Wheat (2)</span>
            </a>
          </div>
        </div>
      </div>
      <!-- Tags End -->
    </div>
  </div>