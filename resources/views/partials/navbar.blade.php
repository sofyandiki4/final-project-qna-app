
    <div class="nav-content d-flex justify-content-between">
      <!-- Logo Start -->
      <div class="logo position-relative">
        <a href="Dashboards.Default.html">
          <!-- Logo can be added directly -->
          <!-- <img src="img/logo/logo-white.svg" alt="logo" /> -->

          <!-- Or added via css to provide different ones for different color themes -->
          <div class="img"></div>
        </a>
      </div>
      <!-- Logo End -->

      <!-- Language Switch Start -->

      <!-- Language Switch End -->

      <!-- User Menu Start -->
      <div class="user-container d-flex ">
        <a href="#" class="d-flex user position-relative" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <img class="profile" alt="profile" src="{{ asset('/template/img/profile/profile-9.webp') }}" />
          <div class="name">Lisa Jackson</div>
        </a>
        <div class="dropdown-menu dropdown-menu-end user-menu wide">

          <div class="row mb-1 ms-0 me-0">
            <div class="col-12 p-1 mb-3 pt-3">
              <div class="separator-light"></div>
            </div>

            <div class="col-6 pe-1 ps-1">
              <ul class="list-unstyled">
                <li>
                  <a href="#">
                    <i data-acorn-icon="gear" class="me-2" data-acorn-size="17"></i>
                    <span class="align-middle">Settings</span>
                  </a>
                </li>
                <li>
                  <a href="{{ url('/logout') }}">
                    <i data-acorn-icon="logout" class="me-2" data-acorn-size="17"></i>
                    <span class="align-middle">Logout</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!-- User Menu End -->

      <!-- Icons Menu Start -->

      <!-- Icons Menu End -->

      <!-- Menu Start -->

      <!-- Menu End -->

      <!-- Mobile Buttons Start -->
      <div class="mobile-buttons-container">
        <!-- Scrollspy Mobile Button Start -->
        <a href="#" id="scrollSpyButton" class="spy-button" data-bs-toggle="dropdown">
          <i data-acorn-icon="menu-dropdown"></i>
        </a>
        <!-- Scrollspy Mobile Button End -->

        <!-- Scrollspy Mobile Dropdown Start -->
        <div class="dropdown-menu dropdown-menu-end" id="scrollSpyDropdown"></div>
        <!-- Scrollspy Mobile Dropdown End -->

        <!-- Menu Button Start -->
        <a href="#" id="mobileMenuButton" class="menu-button">
          <i data-acorn-icon="menu"></i>
        </a>
        <!-- Menu Button End -->
      </div>
      <!-- Mobile Buttons End -->
    </div>
    <div class="nav-shadow"></div>
