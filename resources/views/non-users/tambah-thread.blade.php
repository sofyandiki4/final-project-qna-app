@extends('layouts.master')

@section('title')
Home
@endsection

@section('leftrow-top')
<div class="card-body row g-0">
    <div class="col-12">
        <div class="cta-3">Ingin membuat forum atau berkomentar?</div>
        <div class="mb-3 cta-3 text-primary">Daftarkan akunmu segera!</div>
        <div class="text-muted mb-3">Buat kaun dan kamu bisa memulai thread kamu sendiri.</div>
        <div class="d-flex flex-column justify-content-start">

        </div>
        <a href="#" class="btn btn-icon btn-icon-start btn-primary">
            <i data-acorn-icon="chevron-right"></i>
            <span>Register</span>
        </a>
    </div>
</div>
@endsection

@section('category')
Gaming
@endsection

@section('category-link')
/category
@endsection

@section('thread')
XBOX bangkrut
@endsection

@section('content')

<div class="card mb-5">
    <!-- Content Start -->
    <div class="card-body p-0">

        <!-- Contact Start -->
        <section class="scroll-section" id="contact">
            <h2 class="small-title">Contact</h2>
            <form action="{{ url('/threads') }}" method="POST" enctype="multipart/form-data" class="card mb-5 tooltip-end-top" id="contactForm" novalidate>
                @csrf
                <div class="card-body">
                    <p class="text-alternate mb-4">We would love to hear from you!</p>
                    <div class="mb-3 filled">
                        <input type="text" class="form-control" placeholder="Judul" name="judul" />
                    </div>
                    @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <textarea name="konten" class="form-control pb-2" id="" cols="30" rows="10"></textarea>
                    
                    @error('konten')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="mb-3 filled">
                        
                        <input type="file" class="form-control" placeholder="Input your image" name="img" />
                    </div>

                    <div class="col-12 col-sm-6 col-xl-4">
                        <div class="w-100">
                            <label class="form-label">Kategori</label>
                            <select id="select2Basic" class="form-control" name="kategori_Id">
                                <option label="&nbsp;"></option>
                                @forelse ($kategori as $key => $item)
                                <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                @empty
                                <option value="Breadstick">Kosong</option>
                                @endforelse
                            </select>
                        </div>
                    </div>

                    @error('kategori_Id')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="card-footer border-0 pt-0 d-flex justify-content-end align-items-center">
                    <div>
                        <button class="btn btn-icon btn-icon-end btn-primary" type="submit">
                            <span>Send</span>
                            <i data-acorn-icon="chevron-right"></i>
                        </button>
                    </div>
                </div>
            </form>
        </section>
        <!-- Contact End -->
    </div>
    <!-- Content End -->


</div>

<!-- About the Author Start -->

<!-- About the Author End -->

@endsection
@push('script')

@endpush