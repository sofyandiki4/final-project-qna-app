@extends('layouts.master')

@section('title')
Home
@endsection

@section('leftrow-top')
<div class="card-body row g-0">
    <div class="col-12">
        <div class="cta-3">Ingin membuat forum atau berkomentar?</div>
        <div class="mb-3 cta-3 text-primary">Daftarkan akunmu segera!</div>
        <div class="text-muted mb-3">Buat kaun dan kamu bisa memulai thread kamu sendiri.</div>
        <div class="d-flex flex-column justify-content-start">

        </div>
        <a href="#" class="btn btn-icon btn-icon-start btn-primary">
            <i data-acorn-icon="chevron-right"></i>
            <span>Buat Thread</span>
        </a>
    </div>
</div>
@endsection

@section('category')
Gaming
@endsection

@section('category-link')
/category
@endsection

@section('thread')
{{ $pertanyaan->judul }}
@endsection

@section('content')

<div class="card mb-5">
    <!-- Content Start -->
    <div class="card-body p-0">

        <div class="card-body pt-0">
            <h4 class="mb-3 mt-5">{{ $pertanyaan->judul }}</h4>
            <div>
                <img src="{{ asset('thumbnail/' . $pertanyaan->img) }}" class="rounded mx-auto d-block" alt="...">
                <p>{{ $pertanyaan->konten }}</p>
            </div>
        </div>
    </div>
    <!-- Content End -->

    <div class="card-footer border-0 pt-0">
        <div class="row align-items-center">
            <!-- Comments and Likes Start -->
            <div class="col-6 text-muted">
                <div class="row g-0">
                    <div class="col-auto pe-3">
                        <i data-acorn-icon="eye" class="text-primary me-1" data-acorn-size="15"></i>
                        <span class="align-middle">421</span>
                    </div>
                    <div class="col">
                        <i data-acorn-icon="message" class="text-primary me-1" data-acorn-size="15"></i>
                        <span class="align-middle">4</span>
                    </div>
                </div>
            </div>
            <!-- Comments and Likes End -->

            <!-- Social Buttons Start -->
            <div class="col-6">
                <div class="d-flex align-items-center justify-content-end">
                    <button class="btn btn-sm btn-icon btn-icon-only btn-outline-primary ms-1" type="button">
                        <i data-acorn-icon="facebook"></i>
                    </button>
                    <button class="btn btn-sm btn-icon btn-icon-only btn-outline-primary ms-1" type="button">
                        <i data-acorn-icon="twitter"></i>
                    </button>
                </div>
            </div>
            <!-- Social Buttons End -->
        </div>
    </div>
</div>

<div class="card mb-5">
    <div class="card-body ">
        <form action="/thread/{{ $pertanyaan->id }}" method="POST">
            @csrf
            @method('delete')
            <a class="btn btn-primary" href="/thread/{{ $pertanyaan->id }}/edit" role="button">Edit Thread</a>
            <input type="submit" value="delete" class="btn btn-danger ">
        </form>

    </div>



</div>


<!-- About the Author Start -->
@forelse ($pertanyaan->jawaban as $item)
<h2 class="small-title">comment</h2>
<div class="card mb-5">
    <div class="card-body">
        <div class="row g-0">
            <div class="col">
                <a href="#">{{ $item->user->username }}</a>
                <div class="text-muted text-small mb-2">Development Lead</div>
                <div class="text-medium text-alternate mb-1 clamp-line" data-line="2">
                    {{ $item->jawab }}
                </div>
            </div>
        </div>
    </div>

    <div class="card-footer border-0 pt-0">
        <div class="row align-items-center">
            <!-- Comments and Likes Start -->
            <div class="col-6 text-muted">
                <div class="row g-0">
                    <div class="col-auto pe-3">
                        <i data-acorn-icon="eye" class="text-primary me-1" data-acorn-size="15"></i>
                        <span class="align-middle">421</span>
                    </div>
                    <div class="col">
                        <i data-acorn-icon="message" class="text-primary me-1" data-acorn-size="15"></i>
                        <span class="align-middle">4</span>
                    </div>
                </div>
            </div>
            <!-- Comments and Likes End -->

            <!-- Social Buttons Start -->
            <div class="col-6">
                <div class="d-flex align-items-center justify-content-end">
                    <button class="btn btn-sm btn-icon btn-icon-only btn-outline-primary ms-1 pr-4" type="button">
                        <i data-acorn-icon="arrow-top"></i>
                    </button>
                    <span class="align-middle p-2">4</span>
                    <button class="btn btn-sm btn-icon btn-icon-only btn-outline-primary ms-1" type="button">
                        <i data-acorn-icon="arrow-bottom"></i>
                    </button>
                    <span class="align-middle p-2">4</span>
                </div>
            </div>
            <!-- Social Buttons End -->
        </div>
    </div>
</div>
@empty
    
@endforelse

<!-- About the Author End -->


<div class="card mb-5">
    <!-- Content Start -->
    <div class="card-body p-0">

        <!-- Contact Start -->
        <section class="scroll-section" id="contact">
            <h2 class="small-title">Contact</h2>
            <form action="/comment/{{ $pertanyaan->id }}" method="POST" enctype="multipart/form-data"
                class="card mb-5 tooltip-end-top" id="contactForm" novalidate>
                @csrf
                <div class="card-body">
                    <p class="text-alternate mb-4">We would love to hear from you!</p>


                    <textarea name="jawab" class="form-control pb-2" id="" cols="30" rows="10"></textarea>

                    @error('jawab')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror


                </div>
                <div class="card-footer border-0 pt-0 d-flex justify-content-end align-items-center">
                    <div>
                        <button class="btn btn-icon btn-icon-end btn-primary" type="submit">
                            <span>Send</span>
                            <i data-acorn-icon="chevron-right"></i>
                        </button>
                    </div>
                </div>
            </form>
        </section>
        <!-- Contact End -->
    </div>
    <!-- Content End -->


</div>



</div>
@endsection
