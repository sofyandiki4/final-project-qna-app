@extends('layouts.master')

@section('title')
Home
@endsection

@section('leftrow-top')
<div class="card-body row g-0">
    <div class="col-12">
      <div class="cta-3">Ingin membuat forum atau berkomentar?</div>
      <div class="mb-3 cta-3 text-primary">Daftarkan akunmu segera!</div>
      <div class="text-muted mb-3">Buat kaun dan kamu bisa memulai thread kamu sendiri.</div>
      <div class="d-flex flex-column justify-content-start">

      </div>
      <a href="#" class="btn btn-icon btn-icon-start btn-primary">
        <i data-acorn-icon="chevron-right"></i>
        <span>Register</span>
      </a>
    </div>
  </div>
@endsection

@section('category')
Gaming
@endsection

@section('category-link')
/category
@endsection

@section('thread')
XBOX bangkrut
@endsection

@section('content')

<div class="card mb-5">
    <!-- Content Start -->
    <div class="card-body p-0">

        <div class="card-body pt-0">
            <h4 class="mb-3 mt-5">Carrot Cake Gingerbread</h4>
            <div>
                <p>
                    Toffee croissant icing toffee. Sweet roll chupa chups marshmallow muffin liquorice chupa chups
                    soufflé bonbon. Liquorice gummi bears
                    cake donut chocolate lollipop gummi bears. Cotton candy cupcake ice cream gummies dessert muffin
                    chocolate jelly. Danish brownie
                    chocolate bar lollipop cookie tootsie roll candy canes. Jujubes lollipop cheesecake gummi bears
                    cheesecake. Cake jujubes soufflé.
                </p>
                <p>
                    Cake chocolate bar biscuit sweet roll liquorice jelly jujubes. Gingerbread icing macaroon bear claw
                    jelly toffee. Chocolate cake
                    marshmallow muffin wafer. Pastry cake tart apple pie bear claw sweet. Apple pie macaroon sesame
                    snaps cotton candy jelly
                    <u>pudding lollipop caramels</u>
                    marshmallow. Powder halvah dessert ice cream. Carrot cake gingerbread chocolate cake tootsie roll.
                    Oat cake jujubes jelly-o jelly chupa
                    chups lollipop jelly wafer soufflé.
                </p>
                <h6 class="mb-3 mt-5 text-alternate">Sesame Snaps Lollipop Macaroon</h6>
                <p>
                    Jelly-o jelly oat cake cheesecake halvah. Cupcake sweet roll donut. Sesame snaps lollipop macaroon.
                    <a href="#">Icing tiramisu</a>
                    oat cake chocolate cake marzipan pudding danish gummies. Dragée liquorice jelly beans jelly jelly
                    sesame snaps brownie. Cheesecake
                    chocolate cake sweet roll cupcake dragée croissant muffin. Lemon drops cupcake croissant liquorice
                    donut cookie cake. Gingerbread
                    biscuit bear claw marzipan tiramisu topping. Jelly-o croissant chocolate bar gummi bears dessert
                    lemon drops gingerbread croissant.
                    Donut candy jelly.
                </p>
                <ul class="list-unstyled">
                    <li>Croissant</li>
                    <li>Sesame snaps</li>
                    <li>Ice cream</li>
                    <li>Candy canes</li>
                    <li>Lemon drops</li>
                </ul>
                <h6 class="mb-3 mt-5 text-alternate">Muffin Sweet Roll Apple Pie</h6>
                <p>
                    Carrot cake gummi bears wafer sesame snaps soufflé cheesecake cheesecake cake. Sweet roll apple pie
                    tiramisu bonbon sugar plum muffin
                    sesame snaps chocolate. Lollipop sweet roll gingerbread halvah sesame snaps powder. Wafer halvah
                    chocolate soufflé icing. Cotton candy
                    danish lollipop jelly-o candy caramels.
                </p>
            </div>
        </div>
    </div>
    <!-- Content End -->

    <div class="card-footer border-0 pt-0">
        <div class="row align-items-center">
            <!-- Comments and Likes Start -->
            <div class="col-6 text-muted">
                <div class="row g-0">
                    <div class="col-auto pe-3">
                        <i data-acorn-icon="eye" class="text-primary me-1" data-acorn-size="15"></i>
                        <span class="align-middle">421</span>
                    </div>
                    <div class="col">
                        <i data-acorn-icon="message" class="text-primary me-1" data-acorn-size="15"></i>
                        <span class="align-middle">4</span>
                    </div>
                </div>
            </div>
            <!-- Comments and Likes End -->

            <!-- Social Buttons Start -->
            <div class="col-6">
                <div class="d-flex align-items-center justify-content-end">
                    <button class="btn btn-sm btn-icon btn-icon-only btn-outline-primary ms-1" type="button">
                        <i data-acorn-icon="facebook"></i>
                        
                    </button>
                    <button class="btn btn-sm btn-icon btn-icon-only btn-outline-primary ms-1" type="button">
                        <i data-acorn-icon="twitter"></i>
                    </button>
                </div>
            </div>
            <!-- Social Buttons End -->
        </div>
    </div>
</div>

<!-- About the Author Start -->
<h2 class="small-title">comment</h2>
<div class="card mb-5">
    <div class="card-body">
        <div class="row g-0">
            <div class="col">
                <a href="#">Olli Hawkins</a>
                <div class="text-muted text-small mb-2">Development Lead</div>
                <div class="text-medium text-alternate mb-1 clamp-line" data-line="2">
                    Cupcake chocolate cake jelly beans lemon drops danish bear claw carrot cake soufflé. Marshmallow
                    jujubes tiramisu apple pie carrot cake
                    danish candy. Croissant croissant chocolate bar. Jelly macaroon apple pie croissant pastry
                    cheesecake. Marshmallow oat cake lemon drops
                    chocolate bonbon powder lemon drops chocolate. Danish tootsie roll dessert soufflé.
                </div>
            </div>
        </div>
    </div>

    <div class="card-footer border-0 pt-0">
        <div class="row align-items-center">
            <!-- Comments and Likes Start -->
            <div class="col-6 text-muted">
                <div class="row g-0">
                    <div class="col-auto pe-3">
                        <i data-acorn-icon="eye" class="text-primary me-1" data-acorn-size="15"></i>
                        <span class="align-middle">421</span>
                    </div>
                    <div class="col">
                        <i data-acorn-icon="message" class="text-primary me-1" data-acorn-size="15"></i>
                        <span class="align-middle">4</span>
                    </div>
                </div>
            </div>
            <!-- Comments and Likes End -->

            <!-- Social Buttons Start -->
            <div class="col-6">
                <div class="d-flex align-items-center justify-content-end">
                    <button class="btn btn-sm btn-icon btn-icon-only btn-outline-primary ms-1" type="button">
                        <i data-acorn-icon="facebook"></i>
                    </button>
                    <button class="btn btn-sm btn-icon btn-icon-only btn-outline-primary ms-1" type="button">
                        <i data-acorn-icon="twitter"></i>
                    </button>
                </div>
            </div>
            <!-- Social Buttons End -->
        </div>
    </div>
</div>
<!-- About the Author End -->

@endsection
