@extends('layouts.master')

@section('title')
Home
@endsection

@section('leftrow-top')
<div class="card-body row g-0">
    <div class="col-12">
        <div class="cta-3">Ingin membuat forum atau berkomentar?</div>
        <div class="mb-3 cta-3 text-primary">Daftarkan akunmu segera!</div>
        <div class="text-muted mb-3">Buat kaun dan kamu bisa memulai thread kamu sendiri.</div>
        <div class="d-flex flex-column justify-content-start">

        </div>
        <a href="#" class="btn btn-icon btn-icon-start btn-primary">
            <i data-acorn-icon="chevron-right"></i>
            <span>Register</span>
        </a>
    </div>
</div>
@endsection

@section('category')
Gaming
@endsection

@section('category-link')
/category
@endsection

@section('thread')
XBOX bangkrut
@endsection

@section('content')

<div class="card mb-5">
    <!-- Content Start -->
    <div class="card-body p-0">

        <!-- Contact Start -->
        <section class="scroll-section" id="contact">
            <h2 class="small-title">Contact</h2>
            <form action="{{ url('/thread') }}" method="POST" enctype="multipart/form-data" class="card mb-5 tooltip-end-top" id="contactForm" novalidate>
                @csrf
                <div class="card-body">
                    <p class="text-alternate mb-4">We would love to hear from you!</p>
                    <div class="mb-3 filled">
                        <i data-acorn-icon="user"></i>
                        <input type="text" class="form-control" placeholder="Name" name="contactName" />
                    </div>
                    <textarea name="contentDesc" class="form-control" id="" cols="30" rows="10"></textarea>
                    <div class="mb-3 filled">
                        <i data-acorn-icon="phone"></i>
                        <input type="file" class="form-control" placeholder="Input your image" name="image" />
                    </div>
                    <div class="col-12 col-sm-6 col-xl-4">
                        <div class="w-100">
                            <label class="form-label">Breads</label>
                            <select id="select2Basic" class="form-control" name="category">
                                <option label="&nbsp;"></option>
                                <option value="Breadstick">Breadstick</option>
                                <option value="Biscotti">Biscotti</option>
                                <option value="Fougasse">Fougasse</option>
                                <option value="Lefse">Lefse</option>
                                <option value="Melonpan">Melonpan</option>
                                <option value="Naan">Naan</option>
                                <option value="Panbrioche">Panbrioche</option>
                                <option value="Rewena">Rewena</option>
                                <option value="Shirmal">Shirmal</option>
                                <option value="Tunnbröd">Tunnbröd</option>
                                <option value="Vánočka">Vánočka</option>
                                <option value="Zopf">Zopf</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card-footer border-0 pt-0 d-flex justify-content-end align-items-center">
                    <div>
                        <button class="btn btn-icon btn-icon-end btn-primary" type="submit">
                            <span>Send</span>
                            <i data-acorn-icon="chevron-right"></i>
                        </button>
                    </div>
                </div>
            </form>
        </section>
        <!-- Contact End -->
    </div>
    <!-- Content End -->

    <div class="card-footer border-0 pt-0">
        <div class="row align-items-center">
            <!-- Comments and Likes Start -->
            <div class="col-6 text-muted">
                <div class="row g-0">
                    <div class="col-auto pe-3">
                        <i data-acorn-icon="eye" class="text-primary me-1" data-acorn-size="15"></i>
                        <span class="align-middle">421</span>
                    </div>
                    <div class="col">
                        <i data-acorn-icon="message" class="text-primary me-1" data-acorn-size="15"></i>
                        <span class="align-middle">4</span>
                    </div>
                </div>
            </div>
            <!-- Comments and Likes End -->

            <!-- Social Buttons Start -->
            <div class="col-6">
                <div class="d-flex align-items-center justify-content-end">
                    <button class="btn btn-sm btn-icon btn-icon-only btn-outline-primary ms-1" type="button">
                        <i data-acorn-icon="facebook"></i>

                    </button>
                    <button class="btn btn-sm btn-icon btn-icon-only btn-outline-primary ms-1" type="button">
                        <i data-acorn-icon="twitter"></i>
                    </button>
                </div>
            </div>
            <!-- Social Buttons End -->
        </div>
    </div>
</div>

<!-- About the Author Start -->
<h2 class="small-title">comment</h2>
<div class="card mb-5">
    <div class="card-body">
        <div class="row g-0">
            <div class="col">
                <a href="#">Olli Hawkins</a>
                <div class="text-muted text-small mb-2">Development Lead</div>
                <div class="text-medium text-alternate mb-1 clamp-line" data-line="2">
                    Cupcake chocolate cake jelly beans lemon drops danish bear claw carrot cake soufflé. Marshmallow
                    jujubes tiramisu apple pie carrot cake
                    danish candy. Croissant croissant chocolate bar. Jelly macaroon apple pie croissant pastry
                    cheesecake. Marshmallow oat cake lemon drops
                    chocolate bonbon powder lemon drops chocolate. Danish tootsie roll dessert soufflé.
                </div>
            </div>
        </div>
    </div>

    <div class="card-footer border-0 pt-0">
        <div class="row align-items-center">
            <!-- Comments and Likes Start -->
            <div class="col-6 text-muted">
                <div class="row g-0">
                    <div class="col-auto pe-3">
                        <i data-acorn-icon="eye" class="text-primary me-1" data-acorn-size="15"></i>
                        <span class="align-middle">421</span>
                    </div>
                    <div class="col">
                        <i data-acorn-icon="message" class="text-primary me-1" data-acorn-size="15"></i>
                        <span class="align-middle">4</span>
                    </div>
                </div>
            </div>
            <!-- Comments and Likes End -->

            <!-- Social Buttons Start -->
            <div class="col-6">
                <div class="d-flex align-items-center justify-content-end">
                    <button class="btn btn-sm btn-icon btn-icon-only btn-outline-primary ms-1" type="button">
                        <i data-acorn-icon="facebook"></i>
                    </button>
                    <button class="btn btn-sm btn-icon btn-icon-only btn-outline-primary ms-1" type="button">
                        <i data-acorn-icon="twitter"></i>
                    </button>
                </div>
            </div>
            <!-- Social Buttons End -->
        </div>
    </div>
</div>
<!-- About the Author End -->

@endsection
@push('script')
<script src="https://cdn.ckeditor.com/ckeditor5/36.0.1/classic/ckeditor.js"></script>
<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .catch( error => {
            console.error( error );
        } );
</script>

@endpush