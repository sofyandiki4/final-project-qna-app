@extends('layouts.master')

@section('title')
Home
@endsection

@section('category')
Gaming
@endsection

@section('leftrow-top')
<div class="card-body row g-0">
    <div class="col-12">
        <div class="cta-3">Ingin membuat forum atau berkomentar?</div>
        <div class="mb-3 cta-3 text-primary">Daftarkan akunmu segera!</div>
        <div class="text-muted mb-3">Buat kaun dan kamu bisa memulai thread kamu sendiri.</div>
        <div class="d-flex flex-column justify-content-start">

        </div>
        <a href="#" class="btn btn-icon btn-icon-start btn-primary">
            <i data-acorn-icon="chevron-right"></i>
            <span>Register</span>
        </a>
    </div>
</div>
@endsection

@section('content')

<div class="card mb-5">
    <div class="card-body">
      <div class="d-flex align-items-center flex-column mb-4">
        <div class="d-flex align-items-center flex-column">
          <div class="sw-13 position-relative mb-3">
            <img src="{{ asset('/template/img/profile/profile-2.webp') }}" class="img-fluid rounded-xl" alt="thumb" />
          </div>
          <div class="h5 mb-0">Username</div>
          <div class="text-muted">Executive UI/UX Designer</div>
          <div class="text-muted">
            <i data-acorn-icon="at-sign" class="me-1"></i>
            <span class="align-middle">Email@test.com</span>
          </div>
        </div>
      </div>

    </div>
    
    <div class="col m-5 mt-1">
      <div class="h-100">
        <div class="d-flex flex-column justify-content-start">
          <div class="d-flex flex-column">
            <a href="#" class="heading stretched-link">Alamat</a>
            <div class="text-muted mt-1">
              Deskripsi
            </div>
          </div>
        </div>
      </div>
    </div>
  
  </div>


  




@endsection
