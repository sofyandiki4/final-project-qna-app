@extends('layouts.master')

@section('title')
Home
@endsection

@section('leftrow-top')
<div class="card-body row g-0">
    <div class="col-12">
      <div class="cta-3">Ingin membuat forum atau berkomentar?</div>
      <div class="mb-3 cta-3 text-primary">Daftarkan akunmu segera!</div>
      <div class="text-muted mb-3">Buat kaun dan kamu bisa memulai thread kamu sendiri.</div>
      <div class="d-flex flex-column justify-content-start">

      </div>
      <a href="#" class="btn btn-icon btn-icon-start btn-primary">
        <i data-acorn-icon="chevron-right"></i>
        <span>Register</span>
      </a>
    </div>
  </div>
@endsection

@section('content')

<div class="card mb-5">
    <div class="card-body">
      <h4 class="mb-3">
        <a href="Pages.Blog.Detail.html">Basic Introduction to Bread Making</a>
      </h4>
      <p class="text-alternate clamp-line mb-0" data-line="2">
        Jujubes brownie marshmallow apple pie donut ice cream jelly-o jelly-o gummi bears. Tootsie roll chocolate bar dragée bonbon cheesecake
        icing. Danish wafer donut cookie caramels gummies topping.
      </p>
    </div>
    <div class="card-footer border-0 pt-0">
      <div class="row align-items-center">
        <div class="col-6">
          <div class="d-flex align-items-center">
            <div class="sw-5 d-inline-block position-relative me-3">
              <img src="{{ asset('/template/img/profile/profile-1.webp') }}" class="img-fluid rounded-xl" alt="thumb" />
            </div>
            <div class="d-inline-block">
              <a href="#">Olli Hawkins</a>
              <div class="text-muted text-small">Development Lead</div>
            </div>
          </div>
        </div>
        <div class="col-6 text-muted">
          <div class="row g-0 justify-content-end">
            <div class="col-auto pe-3">
              <i data-acorn-icon="eye" class="text-primary me-1" data-acorn-size="15"></i>
              <span class="align-middle">421</span>
            </div>
            <div class="col-auto">
              <i data-acorn-icon="message" class="text-primary me-1" data-acorn-size="15"></i>
              <span class="align-middle">4</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <nav aria-label="Page navigation example">
    <ul class="pagination justify-content-center">
      <li class="page-item">
        <a class="page-link" href="#" aria-label="First">
          <i data-acorn-icon="arrow-end-left"></i>
        </a>
      </li>
      <li class="page-item">
        <a class="page-link" href="#" aria-label="Previous">
          <i data-acorn-icon="arrow-left"></i>
        </a>
      </li>
      <li class="page-item"><a class="page-link" href="#">1</a></li>
      <li class="page-item active"><a class="page-link" href="#">2</a></li>
      <li class="page-item"><a class="page-link" href="#">3</a></li>
      <li class="page-item">
        <a class="page-link" href="#" aria-label="Next">
          <i data-acorn-icon="arrow-right"></i>
        </a>
      </li>
      <li class="page-item">
        <a class="page-link" href="#" aria-label="Last">
          <i data-acorn-icon="arrow-end-right"></i>
        </a>
      </li>
    </ul>
  </nav>

@endsection
